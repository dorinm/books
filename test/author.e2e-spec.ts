import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/api/app.module';
import { ValidationPipe } from '@nestjs/common';

describe('AuthorController (e2e)', () => {
  let app: INestApplication;
  let payload: any;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  it('/api/v1.0/authors (GET) - get all authors', () => {
    return request(app.getHttpServer())
      .get('/api/v1.0/authors')
      .expect(200);
  });

  it('/api/v1.0/authors (POST) - create author', async () => {
    const body = {
      first_name: 'Ion',
      last_name: 'Creanga',
      birthday: '2017-06-07T14:34:08.700Z',
    };

    payload = await request(app.getHttpServer())
      .post('/api/v1.0/authors')
      .send(body)
      .expect(201)
      .then(res => res.body);

    return expect(payload).toMatchObject(body);
  });

  it('/api/v1.0/authors/:id (GET) - get author by id', async () => {
    const body = {
      first_name: 'Ion',
      last_name: 'Creanga',
      birthday: '2017-06-07T14:34:08.700Z',
    };

    payload = await request(app.getHttpServer())
      .get(`/api/v1.0/authors/${payload._id}`)
      .expect(200)
      .then(res => res.body);

    return expect(payload).toMatchObject(body);
  });

  it('/api/v1.0/authors/:id (GET) - get author by wrong id', async () => {
    const body = {
      _id: '22',
    };

    return request(app.getHttpServer())
      .get(`/api/v1.0/authors/${body._id}`)
      .expect(400)
      .then(res => res.body);
  });

  it('/api/v1.0/authors (GET) - get all authors after created author', async () => {
    const body = {
      first_name: 'Ion',
      last_name: 'Creanga',
      birthday: '2017-06-07T14:34:08.700Z',
    };

    const response = await request(app.getHttpServer())
      .get('/api/v1.0/authors')
      .expect(200)
      .then(res => res.body);

    return expect(response).toEqual(
      expect.arrayContaining([expect.objectContaining(body)]),
    );
  });

  it('/api/v1.0/authors/:id (DELETE) - delete author by id', async () => {
    const response = await request(app.getHttpServer())
      .delete(`/api/v1.0/authors/${payload._id}`)
      .expect(200)
      .then(res => res.body);

    return expect(response).toMatchObject({
      message: `Deleted author ${payload._id}`,
    });
  });
});
