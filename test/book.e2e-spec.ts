import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { ValidationPipe } from '@nestjs/common';

/**
 * Local level
 */
import { AppModule } from '../src/api/app.module';

describe('BookController (e2e)', () => {
  let app: INestApplication;
  let payload: any;
  let authorPayload: any;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  it('/api/v1.0/authors (POST) - create author', async () => {
    const body = {
      first_name: 'Ion',
      last_name: 'Creanga',
      birthday: '2017-06-07T14:34:08.700Z',
    };

    authorPayload = await request(app.getHttpServer())
      .post('/api/v1.0/authors')
      .send(body)
      .expect(201)
      .then(res => res.body);

    return expect(authorPayload).toMatchObject(body);
  });

  it('/api/v1.0/autors/:author_id/books (GET) - get all books of author', () => {
    return request(app.getHttpServer())
      .get(`/api/v1.0/authors/${authorPayload._id}/books`)
      .expect(200);
  });

  it('/api/v1.0/autors/:author_id/books (POST) - create book', async () => {
    const body = {
      title: 'Ion Adentures',
      iban: 'DE75512108100124',
    };

    payload = await request(app.getHttpServer())
      .post(`/api/v1.0/authors/${authorPayload._id}/books`)
      .send(body)
      .expect(201)
      .then(res => res.body);

    return expect(payload).toMatchObject(body);
  });

  it('/api/v1.0/autors/:author_id/books/:id (GET) - get book by id', async () => {
    const body = {
      title: 'Ion Adentures',
      iban: 'DE75512108100124',
    };

    payload = await request(app.getHttpServer())
      .get(`/api/v1.0/authors/${authorPayload._id}/books/${payload._id}`)
      .expect(200)
      .then(res => res.body);

    return expect(payload).toMatchObject(body);
  });

  it('/api/v1.0/autors/:author_id/books/:id (GET) - get book by wrong id', async () => {
    const body = {
      _id: 'VVA',
    };

    return request(app.getHttpServer())
      .get(`/api/v1.0/authors/${authorPayload._id}/books/${body._id}`)
      .expect(400)
      .then(res => res.body);
  });

  it('/api/v1.0/autors/:author_id/books (GET) - get all books after created book', async () => {
    const body = {
      title: 'Ion Adentures',
      iban: 'DE75512108100124',
    };

    const response = await request(app.getHttpServer())
      .get(`/api/v1.0/authors/${authorPayload._id}/books`)
      .expect(200)
      .then(res => res.body);

    return expect(response).toEqual(
      expect.arrayContaining([expect.objectContaining(body)]),
    );
  });

  it('/api/v1.0/books/:id (DELETE) - delete book by id', async () => {
    const response = await request(app.getHttpServer())
      .delete(`/api/v1.0/authors/${authorPayload._id}/books/${payload._id}`)
      .expect(200)
      .then(res => res.body);

    return expect(response).toMatchObject({
      message: `Deleted book ${payload._id}`,
    });
  });
});
