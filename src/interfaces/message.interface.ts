/**
 * Typical response for a crud operation
 */
export interface IMessage {
  /**
   * Status message to return
   */
  message: string;
}
