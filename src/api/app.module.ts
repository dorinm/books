import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleAsyncOptions } from '@nestjs/mongoose';

/**
 * Local level
 */
import { AuthorModule } from './v1.0/author/index';
import { BookModule } from './v1.0/book/index';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) =>
        ({
          uri: configService.get('DATABASE'),
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        } as MongooseModuleAsyncOptions),
    }),
    AuthorModule,
    BookModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
