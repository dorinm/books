import { Schema, Document } from 'mongoose';

/**
 * Schema
 */
export const Author = new Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  birthday: {
    type: Date,
    default: null,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
  books: [{ type: Schema.Types.ObjectId, ref: 'Book' }],
});

/**
 * Author Document
 */
export interface IAuthor extends Document {
  /**
   * ID - UUID
   */
  readonly _id: Schema.Types.ObjectId;

  /**
   * First name
   */
  readonly first_name: string;

  /**
   * Last name
   */
  readonly last_name: string;

  /**
   * Birthday
   */
  readonly birthday: Date;

  /**
   * Created at
   */
  readonly created_at: Date;

  /**
   * Updated at
   */
  readonly updated_at: Date;
}
