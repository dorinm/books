import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength, IsDate } from 'class-validator';
import { Type } from 'class-transformer';

/**
 * Post Author Validation Class
 */
export class PostAuthorValidation {
  /**
   * first_name field
   */
  @ApiProperty({
    required: true,
  })
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(15)
  first_name: string;

  /**
   * last_name field
   */
  @ApiProperty({
    required: true,
  })
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(15)
  last_name: string;

  /**
   * birthday field
   */
  @ApiProperty({
    required: true,
  })
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  birthday: Date;
}
