import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength, IsDate } from 'class-validator';
import { Type } from 'class-transformer';

/**
 * Patch Author Validation Class
 */
export class PatchAuthorValidation {
  /**
   * first_name field
   */
  @ApiProperty({
    required: false,
  })
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(15)
  first_name: string;

  /**
   * last_name field
   */
  @ApiProperty({
    required: false,
  })
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(15)
  last_name: string;

  /**
   * birthday field
   */
  @ApiProperty({
    required: false,
  })
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  birthday: Date;
}
