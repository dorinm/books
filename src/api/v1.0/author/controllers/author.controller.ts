import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Param,
  Patch,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

/**
 * Local level
 */
import { AuthorService } from '../services/author.service';
import { IAuthor } from '../models/author.model';
import { PatchAuthorValidation } from '../validations/patch.author.validation';
import { PostAuthorValidation } from '../validations/post.author.validation';
import { IMessage } from '../../../../interfaces/message.interface';

/**
 * Author Controller
 */
@ApiTags('authors')
@Controller('api/v1.0/authors')
export class AuthorController {
  /**
   * Constructor
   * @param authorService
   */
  constructor(private readonly authorService: AuthorService) {}

  /**
   * Get authors
   * @returns {Promise<Array<IAuthor>>} authors data
   */
  @Get()
  @ApiResponse({ status: 200, description: 'Get authors' })
  @ApiResponse({ status: 400, description: 'Get authors failed' })
  async list(): Promise<Array<IAuthor>> {
    return await this.authorService.list();
  }

  /**
   * Get author by id
   * @param {string} id the id of author to get
   * @returns {Promise<IAuthor>} author data
   */
  @Get(':id')
  @ApiResponse({ status: 200, description: 'Get author' })
  @ApiResponse({ status: 400, description: 'Get author failed' })
  async get(@Param('id') id: string): Promise<IAuthor> {
    try {
      const author = await this.authorService.get(id);

      /**
       *
       */
      if (!author) {
        throw new BadRequestException('The author could not be found.');
      }

      /**
       *
       */
      return author;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  /**
   * Create author
   * @param {PostAuthorValidation} payload
   * @returns {Promise<IAuthor>} author data
   */
  @Post()
  @ApiResponse({ status: 200, description: 'Created author' })
  @ApiResponse({ status: 400, description: 'Create author failed' })
  async post(@Body() payload: PostAuthorValidation): Promise<IAuthor> {
    return await this.authorService.create(payload);
  }

  /**
   * Update author
   * @param {PatchAuthorValidation} payload
   * @returns {Promise<IAuthor>} updated author data
   */
  @Patch(':id')
  @ApiResponse({ status: 200, description: 'Patched author' })
  @ApiResponse({ status: 400, description: 'Patch author failed' })
  async patch(
    @Param('id') id: string,
    @Body() payload: PatchAuthorValidation,
  ): Promise<IAuthor> {
    try {
      return await this.authorService.edit(id, payload);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  /**
   * Remove author
   * @param {string} id the id of author to remove
   * @returns {Promise<IMessage>} whether or not the author has been deleted
   */
  @Delete(':id')
  @ApiResponse({ status: 200, description: 'Deleted author' })
  @ApiResponse({ status: 400, description: 'Delete author failed' })
  async delete(@Param('id') id: string): Promise<IMessage> {
    try {
      return await this.authorService.delete(id);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
}
