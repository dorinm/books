import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

/**
 * Local level
 */
import { Author } from './models/author.model';
import { AuthorService } from './services/author.service';
import { AuthorController } from './controllers/author.controller';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Author', schema: Author }])],
  providers: [AuthorService],
  exports: [AuthorService],
  controllers: [AuthorController],
})
export class AuthorModule {}
