import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

/**
 * Local level
 */
import { IAuthor } from '../models/author.model';
import { PatchAuthorValidation } from '../validations/patch.author.validation';
import { PostAuthorValidation } from '../validations/post.author.validation';
import { IMessage } from '../../../../interfaces/message.interface';
import { IBook } from '../../book/models/book.model';

/**
 * Author Service
 */
@Injectable()
export class AuthorService {
  /**
   * Constructor
   * @param {Model<IAuthor>} authorModel
   */
  constructor(
    @InjectModel('Author') private readonly authorModel: Model<IAuthor>,
  ) {}

  /**
   * Get authors
   * @returns {Promise<Array<IAuthor>>} authors data
   */
  list(): Promise<Array<IAuthor>> {
    return this.authorModel.find().exec();
  }

  /**
   * Get author by id
   * @param {string} id
   * @returns {Promise<IAuthor>} author data
   */
  get(id: string): Promise<IAuthor> {
    return this.authorModel.findById(id).exec();
  }

  /**
   * Create author
   * @param {PostAuthorValidation} payload author payload
   * @returns {Promise<IAuthor>} created author data
   */
  async create(payload: PostAuthorValidation): Promise<IAuthor> {
    const author = new this.authorModel({
      ...payload,
    });

    return author.save();
  }

  /**
   * Attach author book
   * @param {string} id
   * @param {IBook} book book payload
   * @returns {Promise<IAuthor>} author data
   */
  async attachBook(id: string, book: IBook): Promise<IAuthor> {
    await this.authorModel.updateOne(
      { _id: id },
      {
        $addToSet: {
          books: book._id,
        },
      },
    );

    /**
     *
     */
    return this.get(id);
  }

  /**
   * Detach author book
   * @param {string} id
   * @param {IBook} book book payload
   * @returns {Promise<IAuthor>} author data
   */
  async detachBook(id: string, book: IBook): Promise<IAuthor> {
    await this.authorModel.updateOne(
      { _id: id },
      {
        $pull: {
          books: book._id,
        },
      },
    );

    /**
     *
     */
    return this.get(id);
  }

  /**
   * Update author
   * @param {string} id
   * @param {PatchAuthorValidation} payload
   * @returns {Promise<IAuthor>} updated author data
   */
  async edit(id: string, payload: PatchAuthorValidation): Promise<IAuthor> {
    const author = await this.get(id);

    /**
     *
     */
    if (!author) {
      throw new NotFoundException('The author could not be found.');
    }

    await this.authorModel.updateOne({ _id: id }, payload);

    /**
     *
     */
    return this.get(id);
  }

  /**
   * Delete author
   * @param {string} _id
   * @returns {Promise<IMessage>} whether or not the crud operation was completed
   */
  delete(_id: string): Promise<IMessage> {
    return this.authorModel.deleteOne({ _id }).then(author => {
      if (author.deletedCount === 1)
        return { message: `Deleted author ${_id}` };
      throw new BadRequestException(`Failed to delete author ${_id}.`);
    });
  }
}
