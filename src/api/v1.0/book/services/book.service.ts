import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

/**
 * Local level
 */
import { IBook } from '../models/book.model';
import { PatchBookValidation } from '../validations/patch.book.validation';
import { PostBookValidation } from '../validations/post.book.validation';
import { IMessage } from '../../../../interfaces/message.interface';
import { AuthorService } from '../../author/services/author.service';

/**
 * Book Service
 */
@Injectable()
export class BookService {
  /**
   * Constructor
   * @param {Model<IBook>} bookModel
   * @param authorService
   */
  constructor(
    @InjectModel('Book') private readonly bookModel: Model<IBook>,
    private readonly authorService: AuthorService,
  ) {}

  /**
   * Get authors's books
   * @param {string} author
   * @returns {Promise<Array<IBook>>} books data
   */
  list(author: string): Promise<Array<IBook>> {
    return this.bookModel.find({ author }).exec();
  }

  /**
   * Get book by id
   * @param {string} id
   * @returns {Promise<IBook>} book data
   */
  get(id: string): Promise<IBook> {
    return this.bookModel.findById(id).exec();
  }

  /**
   * Get book by iban
   * @param {string} iban
   * @returns {Promise<IBook>} book data
   */
  getByIBAN(iban: string): Promise<IBook> {
    return this.bookModel.findOne({ iban }).exec();
  }

  /**
   * Get book by iban
   * @param {string} iban
   * @returns {Promise<IBook>} book data
   */
  async verifyBookByIBAN(iban: string): Promise<IBook> {
    const book = await this.getByIBAN(iban);

    /**
     *
     */
    if (book) {
      throw new BadRequestException(`The book ${iban} already exists.`);
    }

    return book;
  }

  /**
   * Get book by id
   * @param {string} id
   * @returns {Promise<IBook>} book data
   */
  async verifyBookByID(id: string): Promise<IBook> {
    const book = await this.get(id);

    /**
     *
     */
    if (!book) {
      throw new NotFoundException('The book could not be found.');
    }

    return book;
  }

  /**
   * Create book
   * @param {string} author
   * @param {PostBookValidation} payload book payload
   * @returns {Promise<IBook>} created book data
   */
  async create(author: string, payload: PostBookValidation): Promise<IBook> {
    const { iban } = payload;
    await this.verifyBookByIBAN(iban);

    const book = new this.bookModel({
      ...payload,
      author,
    });

    const saved = book.save();

    /**
     * Attach book to author
     */
    await this.authorService.attachBook(author, book);

    return saved;
  }

  /**
   * Update book
   * @param {string} id
   * @param {PatchBookValidation} payload
   * @returns {Promise<IBook>} updated book data
   */
  async edit(id: string, payload: PatchBookValidation): Promise<IBook> {
    const { iban } = payload;

    await this.verifyBookByID(id);
    await this.verifyBookByIBAN(iban);

    await this.bookModel.updateOne({ _id: id }, payload);

    /**
     *
     */
    return this.get(id);
  }

  /**
   * Remove book
   * @param {string} _id the id of book to remove
   * @returns {Promise<IMessage>} responds if book was deleted or not
   */
  async delete(_id: string): Promise<IMessage> {
    const currentBook = await this.verifyBookByID(_id);
    const { author } = currentBook;

    return this.bookModel.deleteOne({ _id }).then(async book => {
      if (book.deletedCount === 1) {
        /**
         * Detach book to author
         */
        await this.authorService.detachBook(author, currentBook);

        return { message: `Deleted book ${_id}` };
      }
      throw new BadRequestException(`Failed to delete book ${_id}.`);
    });
  }
}
