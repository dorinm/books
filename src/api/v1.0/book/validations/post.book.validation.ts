import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';

/**
 * Post Book Validation Class
 */
export class PostBookValidation {
  /**
   * title field
   */
  @ApiProperty({
    required: true,
  })
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(60)
  title: string;

  /**
   * ibam field
   */
  @ApiProperty({
    required: true,
  })
  @IsNotEmpty()
  @MinLength(10)
  @MaxLength(45)
  iban: string;
}
