import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, IsOptional, MinLength } from 'class-validator';

/**
 * Patch Book Validation Class
 */
export class PatchBookValidation {
  /**
   * title field
   */
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @MinLength(1)
  @MaxLength(60)
  title: string;

  /**
   * iban field
   */
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @MinLength(10)
  @MaxLength(45)
  iban: string;
}
