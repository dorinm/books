import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

/**
 * Local level
 */
import { Book } from './models/book.model';
import { BookService } from './services/book.service';
import { BookController } from './controllers/book.controller';
import { AuthorModule } from '../author/index';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Book', schema: Book }]),
    AuthorModule,
  ],
  providers: [BookService],
  exports: [BookService],
  controllers: [BookController],
})
export class BookModule {}
