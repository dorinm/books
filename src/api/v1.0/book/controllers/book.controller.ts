import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

/**
 * Local level
 */
import { BookService } from '../services/book.service';
import { IBook } from '../models/book.model';
import { PatchBookValidation } from '../validations/patch.book.validation';
import { PostBookValidation } from '../validations/post.book.validation';
import { IMessage } from '../../../../interfaces/message.interface';

/**
 * Book Controller
 */
@ApiTags('books')
@Controller('api/v1.0/authors/:author_id/books')
export class BookController {
  /**
   * Constructor
   * @param bookService
   */
  constructor(private readonly bookService: BookService) {}

  /**
   * Get author books
   * @returns {Promise<Array<IBook>>} books data
   */
  @Get()
  @ApiResponse({ status: 200, description: 'Get books' })
  @ApiResponse({ status: 400, description: 'Get books failed' })
  async list(@Param('author_id') author: string): Promise<Array<IBook>> {
    return await this.bookService.list(author);
  }

  /**
   * Get book by id
   * @param {string} id the id of book to get
   * @returns {Promise<IBook>} book data
   */
  @Get(':id')
  @ApiResponse({ status: 200, description: 'Get book' })
  @ApiResponse({ status: 400, description: 'Get book failed' })
  async get(@Param('id') id: string): Promise<IBook> {
    try {
      const book = await this.bookService.get(id);

      /**
       *
       */
      if (!book) {
        throw new BadRequestException('The book could not be found.');
      }

      /**
       *
       */
      return book;
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  /**
   * Create book
   * @param {PostBookValidation} payload
   * @returns {Promise<IBook>} book data
   */
  @Post()
  @ApiResponse({ status: 200, description: 'Created book' })
  @ApiResponse({ status: 400, description: 'Create book failed' })
  async post(
    @Param('author_id') author: string,
    @Body() payload: PostBookValidation,
  ): Promise<IBook> {
    return await this.bookService.create(author, payload);
  }

  /**
   * Update book
   * @param {PatchBookValidation} payload
   * @returns {Promise<IBook>} updated book data
   */
  @Patch(':id')
  @ApiResponse({ status: 200, description: 'Patched book' })
  @ApiResponse({ status: 400, description: 'Patch book failed' })
  async patch(
    @Param('id') id: string,
    @Body() payload: PatchBookValidation,
  ): Promise<IBook> {
    try {
      return await this.bookService.edit(id, payload);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  /**
   * Remove book
   * @param {string} id the id of book to remove
   * @returns {Promise<IMessage>} responds if book was deleted or not
   */
  @Delete(':id')
  @ApiResponse({ status: 200, description: 'Deleted book' })
  @ApiResponse({ status: 400, description: 'Delete book failed' })
  async delete(@Param('id') id: string): Promise<IMessage> {
    try {
      return await this.bookService.delete(id);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
}
