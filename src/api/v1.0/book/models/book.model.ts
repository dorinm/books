import { Schema, Document } from 'mongoose';

/**
 * Schema
 */
export const Book = new Schema({
  title: { type: String, required: true },
  iban: {
    type: String,
    unique: true,
    required: true,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    default: Date.now,
  },
  author: { type: Schema.Types.ObjectId, ref: 'Author' },
});

/**
 * Book Document
 */
export interface IBook extends Document {
  /**
   * ID - UUID
   */
  readonly _id: Schema.Types.ObjectId;

  /**
   * Title
   */
  readonly title: string;

  /**
   * Author
   */
  readonly author: Schema.Types.ObjectId;

  /**
   * IBan
   */
  readonly iban: string;

  /**
   * Created at
   */
  readonly created_at: Date;

  /**
   * Updated at
   */
  readonly updated_at: Date;
}
