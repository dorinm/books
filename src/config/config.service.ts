import { parse } from 'dotenv';
import * as fs from 'fs';

/**
 * Key-value mapping
 */
export interface EnvConfig {
  [key: string]: string;
}

/**
 * Config Service
 */
export class ConfigService {
  /**
   * Env object
   */
  private readonly envConfig: EnvConfig;

  /**
   * Constructor
   * @param {string} filePath
   */
  constructor(filePath: string) {
    const config = parse(fs.readFileSync(filePath));
    this.envConfig = config;
  }

  /**
   * Get env key value
   * @param {string} key
   * @returns {string} env value
   */
  get(key: string): string {
    return this.envConfig[key];
  }

  /**
   * Check env
   * @param {string} env
   * @returns {boolean} true / false
   */
  isEnv(env: string): boolean {
    return this.envConfig.APP_ENV === env;
  }
}
